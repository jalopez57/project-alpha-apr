from django.shortcuts import redirect
from django.views.generic.edit import CreateView, UpdateView
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.list import ListView


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create_task.html"
    fields = [
        "name",
        "start_date",
        "due_date",
        "project",
        "assignee",
    ]

    def form_valid(self, form):
        task = form.save(commit=False)
        task.assignee = self.request.user
        task.save()
        return redirect("show_project", pk=task.project.id)


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/show_my_tasks.html"
    context_object_name = "tasks"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "tasks/show_my_tasks.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
